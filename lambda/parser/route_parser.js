/* eslint-disable */

const path = require('path');
const fs = require('fs');

let methodsPath = path.join(path.resolve(), '/src/methods');

let data = {};
let endpoints = [];
let methodData = fs.readdirSync(methodsPath);

for (const method of methodData) {
    getFiles(methodsPath, null, method);
}

function getFiles(parentFolder, parentMethod, method) {
    let newMethodsPath;
    if (parentMethod !== null) {
        newMethodsPath = path.join(parentFolder, parentMethod, method);
    } else {
        newMethodsPath = path.join(parentFolder, method);
    }

    let methodIsDirectory = fs.lstatSync(newMethodsPath).isDirectory();

    if (methodIsDirectory) {
        let methodData = fs.readdirSync(newMethodsPath);

        if (parentMethod !== null) {
            parentMethod = `${parentMethod}/${method}`;
        } else {
            parentMethod = `${method}`;
        }
        for (const childMethod of methodData) {
            getFiles(parentFolder, parentMethod, childMethod);
        }
    } else {
        if (parentMethod === null) {
            parentMethod = 'root_folder';
        }
        if (!endpoints.hasOwnProperty(parentMethod)) {
            endpoints[parentMethod] = [];
        }

        let trimmedMethod = method.substring(
            method.length - 3, method
        );

        endpoints[parentMethod][trimmedMethod] = [];

        const factoryPath = path.join(newMethodsPath);
        if (path.extname(factoryPath) === '.js') {
            const factory = require(factoryPath);
            for (const key in factory) {
                endpoints[parentMethod][trimmedMethod].push(key);
            }
        }
    }
}

let config = {
    message: 'Read from files',
    dataMock: [],
    contextMock: {}
};

const mocksPath = `${path.resolve()}/tests/mocks`;

for (let folder in endpoints) {
    let populatedData = {
        folder: folder,
        endpoints: []
    };

    for (let file in endpoints[folder]) {
        for (let key in endpoints[folder][file]) {
            let funcName = endpoints[folder][file][key];
            let mocksObject = {
                list: []
            };
            let mocks = null;

            // Default Mocks
            try {
                mocksObject[funcName] = JSON.stringify(require(`${path.resolve()}/tests/mocks/${funcName}.json`))
            } catch (err) {
                mocksObject[funcName] = "{}";
            }
            mocksObject.list.push({
                value: funcName,
                label: funcName
            });

            fs.readdirSync(mocksPath).map((payload) => {
                if (payload.indexOf(`${funcName}_`) !== -1) {
                    let mocksName;
                    try {
                        const mock = require(`${path.resolve()}/tests/mocks/${payload}`);

                        const mocksNameAndExt = payload.split('.');
                        mocksName = mocksNameAndExt[0];

                        mocksObject[mocksName] = JSON.stringify(mock);
                    } catch (err) {
                        mocksObject[mocksName] = "{}";
                    }

                    mocksObject.list.push({
                        value: mocksName,
                        label: mocksName
                    });
                }
            });

            populatedData.endpoints.push({
                path: file,
                function: funcName,
                mocks: mocksObject
            });
        }
    }

    config.dataMock.push(populatedData);
}

const contextFile = [];
fs.readdirSync(mocksPath).map((context) => {
    if (context.toLowerCase().indexOf('context') !== -1) {
        try {
            const mock = require(`${path.resolve()}/tests/mocks/${context}`);

            const nameAndExt = context.split('.');
            const contextName = nameAndExt[0];
            const contextExt = nameAndExt[1];

            config.contextMock[contextName] = JSON.stringify(mock);
        } catch (err) {
            console.log(`Fail to fetch context mock of ${context}`);
        }
    }
});

// preparing no context
config.contextMock.emptyContext = JSON.stringify({});

fs.writeFileSync(
    path.join(__dirname, "routes.json"),
    JSON.stringify(config),
    "utf8"
);

console.log(`√ Success creating files`);