/* eslint-disable */

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const parsedConfig = require('./lambda/parser/routes.json');
const lambda = require('./../../');
const Promise = require('bluebird');
const fs = require('fs');
const path = require('path');

const handler = Promise.promisify(lambda.handler);
const app = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/api/get-sidebar', (req, res) => {
    const items = [];
    const dataMocks = {};
    const contextMock = parsedConfig.contextMock;

    for (const key in parsedConfig.dataMock) {
        const folder = parsedConfig.dataMock[key].folder;
        const endpoints = parsedConfig.dataMock[key].endpoints;
        dataMocks[folder] = {};

        const data = {
            name: folder,
            children: []
        };

        for (let endpointKey in endpoints) {
            const endpointPath = endpoints[endpointKey].path;
            const endpointFunction = endpoints[endpointKey].function;
            dataMocks[folder]
            data.children.push({
                name: endpointFunction,
                url: `/linter?folder=${folder}&path=${endpointPath}&function=${endpointFunction}`
            });

            dataMocks[folder][endpointFunction] = endpoints[endpointKey].mocks;
        }

        items.push(data);
    }

    res.send({
        items,
        dataMocks,
        contextMock
    });
});

app.post('/mocks/data/update', (req, res) => {
    if (req.body) {
        payloadType = req.body.payloadType,
        funcName = req.body.funcName;
        newMocks = req.body.newMocks;
    } else {
        res.status(422).send({
            code: 422,
            data: 'No request body data'
        });
    }

    try {
        fs.writeFileSync(
            `${path.resolve()}/tests/mocks/${funcName}.json`,
            newMocks,
            "utf8"
        );

        res.status(200).send({
            code: 200,
            data: 'Mocks updated'
        });
    } catch (err) {
        res.status(500).send({
            code: 500,
            data: err.message
        });
    }
});

// Registering method paths from lambda functions
for (let key in parsedConfig.dataMock) {
    const endpoints = parsedConfig.dataMock[key].endpoints;

    for (let endpointKey in endpoints) {
        const endpointFunction = endpoints[endpointKey].function;
        const endpointPath = endpoints[endpointKey].path;

        const endpointCompletePath = `/${endpointPath}/${endpointFunction}`;
        app.post(endpointCompletePath, async (req, res, next) => {
            let dataPayload;
            let contextPayload;
            let actionName;
            let fileName;

            if (req.body) {
                dataPayload = req.body.data;
                contextPayload = req.body.context;
                actionName = req.body.actionName;
                fileName = req.body.fileName;
            } else {
                res.status(422).send({
                    code: 422,
                    data: 'No request body data'
                });
            }

            res.status(200).send(dataPayload);

            try {
                return await handler({
                    action: actionName,
                    file: fileName,
                    data: dataPayload,
                    context: contextPayload
                }, {
                        invokedFunctionArn: 'local'
                    })
                    .then(result => {
                        if (typeof result === 'object') {
                            res.status(200).send(result);
                        } else {
                            res.status(200).send(result.toString());
                        }
                    })
                    .catch(error => {
                        if (error.defaultCode) {
                            res.status(error.defaultCode).send({
                                code: error.defaultCode,
                                data: error._message
                            });
                        } else {
                            res.status(500).send({
                                data: error.message,
                                stack: error.stack
                            });
                        }
                    });
            } catch (error) {
                res.status(500).send(error.message);
            }
        });
    }
}

process.on('SIGINT', function() {
    console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );

    process.exit();
});

app.listen(5000, () => console.log(`Listening on port 5000`));