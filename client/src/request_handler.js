import axios from 'axios';

export function getSidebarData(props) {
    axios.get(`http://localhost:5000/api/get-sidebar`)
        .then((res) => {
            props.dispatch({
                type: 'linter_sidebar.has_data',
                data: res.data.items
            });

            props.dispatch({
                type: 'linter_sidebar_mocks.has_data',
                dataMocks: res.data.dataMocks,
                contextMock: res.data.contextMock
            });

            const contextList = Object.keys(res.data.contextMock).map((mock) => {
                return {
                    value: mock,
                    label: mock
                };
            });
            
            props.dispatch({
                type: 'linter_context_list.has_list',
                list: contextList
            });
        })
        .catch((err) => {
            console.log(err);
        });
}

export function submitRequest(path, payload, props) {
    props.dispatch({
        type: 'linter_request.request'
    })
    
    var startTime = new Date().getTime();
    axios.post(`http://localhost:5000${path}`, payload)
        .then((res) => {
            let requestTime = new Date().getTime() - startTime;

            props.dispatch({
                type: 'linter_request.request_success',
                body: res.data,
                code: '-',
                badge: 'success',
                request_time: requestTime
            });
        })
        .catch((err) => {
            let errorMessage = 'Something error';

            if (err.response && err.response.data) {
                errorMessage = err.response.data;
            }

            let requestTime = new Date().getTime() - startTime;

            props.dispatch({
                type: 'linter_request.request_error',
                body: errorMessage,
                code: 0,
                badge: 'danger',
                request_time: requestTime
            })
        })
}

export function updateMocks(payloadType, funcName, newMocks) {
    axios.post(`http://localhost:5000/mocks/data/update`, {
        payloadType,
        funcName,
        newMocks
    })
        .then((res) => {
            console.log(res);
        })
        .catch((err) => {
            console.log(err);
        })
}