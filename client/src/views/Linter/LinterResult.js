import React, { Component } from "react";
import { connect } from "react-redux";
import JSONTree from "react-json-tree";
import PropTypes from "prop-types";
import { Controlled as CodeMirror } from 'react-codemirror2';
import JsonTree from 'react-json-tree';

class LinterResult extends Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);

        this.changeViewMode = this.changeViewMode.bind(this);
    }

    /**
     * Trigger change view
     */
    changeViewMode(event) {
        let viewMode = event.target.getAttribute('data-view');

        this.props.dispatch({
            type: 'linter_view_mode.change_view',
            mode: viewMode
        })
    }

    render() {
        let view;
        let viewMode = this.props.linterViewMode.mode;
        let body = this.props.linterRequest.body;

        if (viewMode === 'raw') {
            if (typeof body === 'object') {
                body = JSON.stringify(body, null, 2);
            } else if (typeof body === 'number') {
                body = body.toString();
            }

            view = <CodeMirror theme='monokai' value={body}
                options={{
                    mode: 'javascript',
                    lineWrapping: true
                }}
            />;
        } else if (viewMode === 'pretty') {
            const theme = {
                scheme: 'bright',
                author: 'chris kempson (http://chriskempson.com)',
                base00: '#000000',
                base01: '#303030',
                base02: '#505050',
                base03: '#b0b0b0',
                base04: '#d0d0d0',
                base05: '#e0e0e0',
                base06: '#f5f5f5',
                base07: '#ffffff',
                base08: '#fb0120',
                base09: '#fc6d24',
                base0A: '#fda331',
                base0B: '#a1c659',
                base0C: '#76c7b7',
                base0D: '#6fb3d2',
                base0E: '#d381c3',
                base0F: '#be643c'
            };

            if (typeof body !== 'object') {
                body = body.toString();

                view = <CodeMirror theme='monokai' value={body}
                    options={{
                        mode: 'javascript',
                        lineWrapping: true
                    }}
                />;

            } else {
                view = <JSONTree hideRoot={false} data={body} theme={theme} />
            }


        }

        return (
            <div className={`card card-accent-${this.props.linterRequest.badge} card-result-wrapper`}>
                <div className="card-header">
                    <button type="button"
                        className="btn btn-sm btn-success float-left"
                        data-balloon="Pretty"
                        data-balloon-pos="up"
                        data-view="pretty"
                        onClick={this.changeViewMode}>
                        <i className="fa fa-list-alt" data-view="pretty"></i>
                    </button>

                    <button type="button"
                        className="btn btn-sm btn-secondary float-lfet"
                        data-balloon="Raw"
                        data-balloon-pos="up"
                        data-view="raw"
                        onClick={this.changeViewMode}
                        style={{ marginRight: 5 + 'px' }}>
                        <i className="fa fa-align-center" data-view="raw"></i>
                    </button>
                    Request Result
                        <span className='badge badge-info float-right'>{this.props.linterRequest.request_time}ms</span>
                    <span className={`badge badge-${this.props.linterRequest.badge} float-right`} style={{ marginRight: 5 + 'px' }}>{this.props.linterRequest.code}</span>
                </div>
                <div style={{ margin: 10 + 'px' }}>
                    {view}
                </div>
            </div>
        );
    }
}

LinterResult = connect((state) => {
    return {
        linterRequest: state.linterRequest,
        linterViewMode: state.linterViewMode
    }
})(LinterResult)

export default LinterResult;