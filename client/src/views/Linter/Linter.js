import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Col, Row } from 'react-bootstrap';
import url from 'url';
import { UnControlled as CodeMirror } from 'react-codemirror2';
import LinterResult from './LinterResult';
import { submitRequest, updateMocks } from './../../request_handler';
import Select from 'react-select';

class Linter extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.useDataMock = this.useDataMock.bind(this);
        this.updateDataMock = this.updateDataMock.bind(this);
        this.useContextMock = this.useContextMock.bind(this);
        this.updateContextMock = this.updateContextMock.bind(this);
        this.updateDataLocalMocks = this.updateDataLocalMocks.bind(this);
        this.updateContextLocalMocks = this.updateContextLocalMocks.bind(this);
    }

    handleSubmit() {
        let data = this.props.linterPayloadData.data;
        let context = this.props.linterContextData.data;

        let fileName = this.query.path;
        let path = `/${this.query.path}/${this.query.function}`;

        try {
            while (typeof data === 'string') {
                data = JSON.parse(data);
            }
        } catch (err) {
            this.props.dispatch({
                type: 'linter_request.request_error',
                body: 'Failed when try to parse payload data to JSON',
                code: '-',
                badge: 'danger',
                request_time: 0
            });
            return;
        }

        try {
            while (typeof context === 'string') {
                context = JSON.parse(context);
            }
        } catch (err) {
            this.props.dispatch({
                type: 'linter_request.request_error',
                body: 'Failed when try to parse payload data to JSON',
                code: '-',
                badge: 'danger',
                request_time: 0
            });
            return;
        }

        let payload = {
            data,
            context,
            fileName,
            actionName: this.query.function
        }

        submitRequest(path, payload, this.props);
    }

    componentWillMount() {
        this.query = null;
        this.query = url.parse(this.props.location.search, true).query;
    }

    componentWillUpdate(newProps) {
        this.query = null;
        this.query = url.parse(newProps.location.search, true).query;
    }

    updateDataMock(editor, data, value) {
        try {
            if (typeof value === 'object') {
                this.props.dispatch({
                    type: 'linter_payload_data.change',
                    data: JSON.stringify(JSON.parse(value), null, 2)
                });
            } else if (typeof value === 'string') {
                this.props.dispatch({
                    type: 'linter_payload_data.change',
                    data: value
                });
            }
        } catch (err) {
            // do nothing
        }
    }

    updateContextMock(editor, data, value) {
        try {
            if (typeof value === 'object') {
                this.props.dispatch({
                    type: 'linter_context_data.change',
                    data: JSON.stringify(JSON.parse(value), null, 2)
                });
            } else if (typeof value === 'string') {
                this.props.dispatch({
                    type: 'linter_context_data.change',
                    data: value
                });
            }
        } catch (err) {
            // do nothing
        }
    }

    useDataMock(selected) {
        const { value: selectedValue } = selected;
        let mocks = null;
        try {
            mocks = this.props.linterSidebarMocks.dataMocks[this.query.folder][this.query.function][selectedValue];
        } catch (err) {
            console.log('Mocks not found');
        }

        this.props.dispatch({
            type: 'linter_context_list.select_payload',
            selected_payload: selectedValue
        });

        this.props.dispatch({
            type: 'linter_payload.use_mocks',
            mocks: JSON.stringify(JSON.parse(mocks), null, 2)
        });

        this.props.dispatch({
            type: 'linter_payload_data.change',
            data: JSON.stringify(JSON.parse(mocks), null, 2)
        });
    }

    useContextMock(selected) {
        this.props.dispatch({
            type: 'linter_context_list.select',
            selected: selected.value
        });

        this.props.dispatch({
            type: 'linter_context.use_mocks',
            mocks: JSON.stringify(JSON.parse(this.props.linterSidebarMocks.contextMock[selected.value]), null, 2)
        });

        this.props.dispatch({
            type: 'linter_context_data.change',
            data: JSON.stringify(JSON.parse(this.props.linterSidebarMocks.contextMock[selected.value]), null, 2)
        });
    }

    updateDataLocalMocks() {
        let newMocks;
        let sidebarDataMocks = this.props.linterSidebarMocks.dataMocks;
        let selectedMocks = this.props.linterContextList.selected_payload;
        try {
            newMocks = JSON.parse(this.props.linterPayloadData.data);
        } catch (err) {
            console.log('Bad JSON');
            return;
        }

        updateMocks(
            'data',
            selectedMocks,
            JSON.stringify(newMocks, null, 2)
        );

        this.props.dispatch({
            type: 'linter_payload.update_mocks',
            mocks: JSON.stringify(newMocks, null, 2)
        });

        sidebarDataMocks[this.query.folder][this.query.function][selectedMocks] = JSON.stringify(newMocks, null, 2);
        this.props.dispatch({
            type: 'linter_sidebar_mocks.update_data_mocks',
            dataMocks: sidebarDataMocks
        });
    }

    updateContextLocalMocks() {
        let newMocks;
        try {
            newMocks = JSON.parse(this.props.linterContextData.data);
        } catch (err) {
            console.log('Bad JSON');
            return;
        }

        updateMocks(
            'context',
            this.props.linterContextList.selected,
            JSON.stringify(newMocks, null, 2)
        );

        this.props.dispatch({
            type: 'linter_context.update_mocks',
            mocks: JSON.stringify(newMocks, null, 2)
        });

        const newSidebarContextMock = this.props.linterSidebarMocks.contextMock;
        newSidebarContextMock[this.props.linterContextList.selected] = JSON.stringify(newMocks);
        this.props.dispatch({
            type: 'linter_sidebar_mocks.update_context_mocks',
            contextMock: newSidebarContextMock
        });
    }

    /**
     * @returns {XML}
     */
    render() {
        let payloadMocksSelect;
        if (this.query.folder !== ''
            && this.query.function !== ''
            && this.props.linterSidebarMocks.dataMocks[this.query.folder]
        ) {
            const folder = this.query.folder;
            const func = this.query.function;
            payloadMocksSelect = <Select
                id="state-select"
                onChange={this.useDataMock}
                value={this.props.linterContextList.selected_payload}
                options={this.props.linterSidebarMocks.dataMocks[folder][func].list}
                clearable={false}
            />;
        } else {
            <Select
                id="state-select"
                onChange={this.useDataMock}
                value={this.props.linterContextList.selected_payload}
                options={[]}
                clearable={false}
            />
        }

        let updateDataMocksButton;
        let updateContextMocksButton;
        if (this.props.linterPayloadMocks.mocks !== this.props.linterPayloadData.data) {
            updateDataMocksButton = <button className='btn btn-md btn-dark float-right'
                style={{ marginRight: 10 + 'px' }}
                onClick={this.updateDataLocalMocks}
                data-balloon="Will update your local file mocks"
                data-balloon-pos="down"
            >Update Mocks</button>;
        }

        if (this.props.linterContextMocks.mocks !== this.props.linterContextData.data) {
            if (this.props.linterContextList.selected !== 'emptyContext') {
                updateContextMocksButton = <button className='btn btn-md btn-dark float-right'
                    style={{ marginRight: 10 + 'px' }}
                    onClick={this.updateContextLocalMocks}
                    data-balloon="Will update your local file mocks"
                    data-balloon-pos="down"
                >Update Mocks</button>;
            }
        }

        // console.log(this.props.linterQueryInformation);
        return (
            <div className="animated fadeIn">
                <Col xs={12}>
                    <div className="card">
                        <div className="card-header">Payloads : <strong>{this.query.function}</strong></div>
                        <Row className="card-block">
                            <Col sm={6} md={6}>
                                <strong><p>Data Payload</p></strong>
                                <div style={{ margin: 10 + 'px' }}>
                                    <CodeMirror
                                        onChange={this.updateDataMock}
                                        options={{
                                            lineNumbers: true
                                        }}
                                        theme='monokai'
                                        value={this.props.linterPayloadMocks.mocks}
                                    />
                                </div>
                                <Row>
                                    <Col sm={6} md={6}>
                                        {updateDataMocksButton}
                                    </Col>
                                    <Col sm={6} md={6}>
                                        {payloadMocksSelect}
                                    </Col>
                                </Row>
                                {/* <button className='btn btn-md btn-info float-right'
                                    onClick={this.useDataMock}
                                    data-balloon="Use mocks from local file"
                                    data-balloon-pos="down"
                                >Use Mocks</button> */}
                            </Col>
                            <Col sm={6} md={6}>
                                <strong><p>Context Payload</p></strong>
                                <div style={{ margin: 10 + 'px' }}>
                                    <CodeMirror
                                        onChange={this.updateContextMock}
                                        options={{
                                            lineNumbers: true
                                        }}
                                        theme='monokai'
                                        value={this.props.linterContextMocks.mocks}
                                    />
                                </div>
                                <Row>
                                    <Col sm={6} md={6}>
                                        {updateContextMocksButton}
                                    </Col>
                                    <Col sm={6} md={6}>
                                        <Select
                                            id="state-select"
                                            onChange={this.useContextMock}
                                            value={this.props.linterContextList.selected}
                                            options={this.props.linterContextList.list}
                                            clearable={false}
                                        />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <div className="card-footer">
                            <button className='btn btn-md btn-info float-right' onClick={this.handleSubmit}>
                                <i className='fa fa-dot-circle-o'></i> Submit
                            </button>
                        </div>
                    </div>
                </Col>

                <Col xs={12}>
                    <LinterResult />
                </Col>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        linterPayloadMocks: state.linterPayloadMocks,
        linterPayloadData: state.linterPayloadData,
        linterContextMocks: state.linterContextMocks,
        linterContextData: state.linterContextData,
        linterContextList: state.linterContextList,
        linterSidebarMocks: state.linterSidebarMocks,
        linterQueryInformation: state.linterQueryInformation
    };
}

export default connect(mapStateToProps)(Linter)
