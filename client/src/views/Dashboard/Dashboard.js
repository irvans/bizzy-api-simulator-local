import React, { Component } from 'react';
import axios from 'axios';

class Dashboard extends Component {
    render() {
        return (
            <div className="animated fadeIn">
                <div className="card">
                    <div className="card-block">
                        Bizzy Simulator for Local
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;
