const routes = {
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/linter': 'Linter'
};

export default routes;
