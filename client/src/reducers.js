import { combineReducers } from 'redux';

import {
    linterPayloadData,
    linterPayloadMocks,
    linterContextData,
    linterContextMocks,
    linterContextList,
    linterSidebar,
    linterSidebarMocks,
    linterRequest,
    linterViewMode
} from './views/Linter/reducer';

const allReducers = {
    linterPayloadMocks,
    linterPayloadData,
    linterContextMocks,
    linterContextData,
    linterContextList,
    linterSidebar,
    linterSidebarMocks,
    linterRequest,
    linterViewMode
}

export default combineReducers(allReducers);