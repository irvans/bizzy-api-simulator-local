How to implement :

- Add to package.json devDependencies: 
    `"bizzy-api-simulator": "git+ssh://git@bitbucket.org/irvans/bizzy-api-simulator-local.git"`, 

- Add to package.json scripts:


    ```"simulator": "node node_modules/bizzy-api-simulator/lambda/parser/route_parser && concurrently --kill-others-on-fail \"npm run simulator-server\" \"npm run simulator-client\"",

   "simulator-client": "cd node_modules/bizzy-api-simulator && npm run client",


    "simulator-server": "forever -w node_modules/bizzy-api-simulator/server.js"```

How to run :  
- Go to your project root  
- Type `npm run simulator`  